<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;

class Resource extends Model
{
    use Translatable;
    protected $guarded = [];
//    public $translationModel = 'App\ResourceTranslation';
    public $translatedAttributes = ['title', 'description'];
    public function category()
    {
        return $this->belongsTo('App\Category');
    }
//    public function getTextAttribute()
//    {
//        if(App::getLocale() == 'ru'){
//            $locale = 'ru';
//        }
//        else{
//            $locale = 'uz';
//        }
//    	return $this->{'title_'.$locale};
//    }
//    public function getDescriptionAttribute()
//    {
//        if(App::getLocale() == 'ru'){
//            $locale = 'ru';
//        }
//        else{
//            $locale = 'uz';
//        }
//        return $this->{'desc_'.$locale};
//    }
}
