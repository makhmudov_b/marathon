<?php

namespace App\Http\Controllers;

use App\Promo;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        $validator = Validator::make(request()->all(), [
                'name' => 'required',
                'last_name' => 'required',
                'email' => 'required|unique:users',
                'phone' => 'required|unique:users',
                'gender' => 'required',
                'country' => 'required',
                'city' => 'required',
                'extra_phone' => 'required',
                'password' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validator)
                        ->withInput(request()->except('password'));
        }

        $user = User::create([
                'distance_id'=> request('distance_id') * 1,
                'name'=> request('name'),
                'last_name'=> request('last_name'),
                'birthday'=> \Carbon\Carbon::parse(request('birthday')),
                'email'=> request('email'),
                'password'=> bcrypt(request('password')),
                'is_paid'=> 0,
                'phone'=> request('phone'),
                'extra_phone'=> request('extra_phone'),
                'gender'=> request('gender'),
                'country'=> request('country'),
                'city'=> request('city'),
                'promo'=> request('promo'),
                'size'=> request('size'),
                'level'=> request('level'),
                'invalid'=> request('invalid') !== null ? request('invalid') : 0,
            ]);
        $promo = Promo::where('name',request('promo'))->first();
        if($promo) {
            if (!$promo->user) {
                $promo->update([
                    'user_id' => $user->id
                ]);
                $user->update([
                    'is_paid' => 1
                ]);
                Auth::login($user);
                return redirect()->action('PageController@success')->with('success', trans('main.sucpromo').'#' . request('promo'));
            }
        }
        Auth::login($user);
        return redirect()->action('PageController@payment');
    }
    public function login()
    {
        $credentials = request()->only('email', 'password');
        if (Auth::attempt($credentials)) {
            return redirect()->action('PageController@index')->with('success',trans('main.suclog'));
        }
        else{
            return redirect()->action('PageController@index')->with('error',trans('main.errlog'));
        }
    }
    public function update(Request $request){
        if(User::first()->phone == 0 || true) {
            if (request('new_password')) {
                $this->validate($request, [
                    'password' => 'same:password',
                    'new_password' => 'confirmed|different:password',
                    'email' => 'required|unique:users,email,' . Auth::user()->id,
                    'name' => 'required',
                    'last_name' => 'required',
                    'phone' => 'required|unique:users',
                    'gender' => 'required',
                    'country' => 'required',
                    'city' => 'required',
                    'extra_phone' => 'required',
                ]);
                Auth::user()->update([
                    'extra_phone' => request('extra_phone'),
                    'email' => request('email'),
                    'phone' => request('phone'),
                    'name' => request('name'),
                    'city' => request('city'),
                    'country' => request('country'),
                    'gender' => request('gender'),
                    'size' => request('size'),
                    'birthday' => request('birthday'),
                    'last_name' => request('last_name'),
                    'password' => bcrypt(request('new_password'))
                ]);
            } else {
                $this->validate($request, [
                    'email' => 'required|unique:users,email,' . Auth::user()->id,
                    'name' => 'required',
                    'last_name' => 'required',
                    'phone' => 'required|unique:users,phone,' . Auth::user()->id,
                    'gender' => 'required',
                    'country' => 'required',
                    'city' => 'required',
                    'extra_phone' => 'required',
                ]);
            }
            Auth::user()->update([
                'extra_phone' => request('extra_phone'),
                'email' => request('email'),
                'phone' => request('phone'),
                'name' => request('name'),
                'city' => request('city'),
                'country' => request('country'),
                'last_name' => request('last_name'),
                'gender' => request('gender'),
                'size' => request('size'),
                'birthday' => request('birthday'),
            ]);
        }
        else{
            $this->validate($request, [
                'password' => 'same:password',
                'new_password' => 'confirmed|different:password',
            ]);
            Auth::user()->update([
                'extra_phone'=>request('extra_phone'),
                'password'=> bcrypt(request('new_password'))
            ]);
        }
        return redirect()->back()->with('success',trans('main.change'));
    }
    public function logout()
    {
        Auth::logout();
        return redirect()->action('PageController@index')->with('success',trans('main.logout'));
    }
}
