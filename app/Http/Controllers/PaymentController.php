<?php

namespace App\Http\Controllers;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Auth;
use App\Distance;
use App\User;
use Illuminate\Support\Facades\Mail;
use App\Mail\ConfirmEmail;

class PaymentController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
        if (!Auth::check()){
            return redirect()->action('PageController@index');
        }
            return $next($request);
        },['except' => [
            'status','sendMail'
        ]]);
    }
    public function octo()
    {
        $user = Auth::user();
        $locale = \App::getLocale();
        if( $locale == 'ru'){
            $text ='Оплата для участия в Полумарафоне';
        }
        if( $locale == 'en'){
            $text = 'Half marathon participation fee';
        }
        if( $locale == 'uz'){
            $text = "Yarim marafonda ishtirok etish uchun to'lov";
        }
        $distance_price = Distance::where('id',$user->distance_id)->first()->price_usd;
        $client = new Client();
        $response = $client->post( 'https://secure.octo.uz/prepare_payment', [
            'headers' => ['Content-Type' => 'application/json','Accept' => 'application/json'],
            'json' => [
                      "octo_shop_id" => 1400,
                      "octo_secret" => "7a5f1866-9df4-44c3-a777-ddc91607ff48",
                      "shop_transaction_id" => time().strval($user->id),
                      "auto_capture" => true,
                      "test" => false,
                      "init_time" => date("Y-m-d H:i:s"),
                      "user_data" => [
                        "user_id" => $user->id,
                        "phone" => $user->phone,
                        "email" => $user->email
                      ],
                      "total_sum" => number_format($distance_price, 2, '.', ''),
                      "currency" => "USD",
                      "description" => $text,
                      "payment_methods" => [
                        [
                          "method" => "bank_card"
                        ]
                      ],
                      "return_url" => "https://samarqand21.uz/register/payment",
                      "notify_url" => "https://samarqand21.uz/api/payment",
                      "language" => $locale,
                      "ttl" => 15
            ]
            ]);
        $data = json_decode($response->getBody()->getContents());
        return redirect($data->octo_pay_url);
    }
    public function uzcard()
    {
        $user = Auth::user();
        $locale = \App::getLocale();
        if( $locale == 'ru'){
            $text ='Оплата для участия в Полумарафоне';
        }
        if( $locale == 'en'){
            $text = 'Half marathon participation fee';
        }
        if( $locale == 'uz'){
            $text = "Yarim marafonda ishtirok etish uchun to'lov";
        }
        $distance_price = Distance::where('id',$user->distance_id)->first()->price;
        $client = new Client();
        $response = $client->post( 'https://secure.octo.uz/prepare_payment', [
            'headers' => ['Content-Type' => 'application/json','Accept' => 'application/json'],
            'json' => [
                      "octo_shop_id" => 1400,
                      "octo_secret" => "7a5f1866-9df4-44c3-a777-ddc91607ff48",
                      "shop_transaction_id" => time().strval($user->id),
                      "auto_capture" => true,
                      "test" => false,
                      "init_time" => date("Y-m-d H:i:s"),
                      "user_data" => [
                        "user_id" => $user->id,
                        "phone" => $user->phone,
                        "email" => $user->email
                      ],
                      "total_sum" => $distance_price*1,
                      "currency" => "UZS",
                      "description" => $text,
                      "payment_methods" => [
                        [
                          "method" => "bank_card"
                        ]
                      ],
                      "return_url" => "https://samarqand21.uz/register/payment",
                      "notify_url" => "https://samarqand21.uz/api/payment",
                      "language" => $locale,
                      "ttl" => 15
            ]
            ]);
        $data = json_decode($response->getBody()->getContents());
        return redirect($data->octo_pay_url);
    }
    public function status()
    {
        $request = request();
        $status = $request->status;
        $transaction_id = $request->shop_transaction_id;
        $length = strlen($transaction_id);
        $user_id = substr($request->shop_transaction_id, 10, $length) * 1;
        $payment_uuid = $request->octo_payment_UUID;
        if($status == 'waiting_for_capture'){
            $response = [
                'accept_status'=> 'capture'
            ];
            return response()->json($response);
        }
        if($status == 'succeeded'){
            $user = User::find($user_id);
            $user->update([
                'is_paid' => 1
            ]);
            Mail::to($user->email)->send(new ConfirmEmail($user));
            return response('ok',200);
        }
    }
    public function sendMail()
    {
        $data = User::find(4);
        Mail::to("sagitov.mirkhan@gmail.com")->send(new ConfirmEmail($data));
        return response('ok',200);
    }
}
