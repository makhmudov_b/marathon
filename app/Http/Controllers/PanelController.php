<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Resource;
use App\User;
use App\Distance;
use App\Category;
use \Mcamara\LaravelLocalization\Facades\LaravelLocalization;
use Intervention\Image\ImageManagerStatic as Image;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\UsersExport;
use App\Exports\UsersExportPaid;
use App\Pages;

class PanelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        $distance = Distance::all();
        return view('backend.index',compact('users','distance'));
    }

    public function import()
    {
        $response = Excel::download(new UsersExport, 'runners_all.xlsx');
        return $response;
    }

    public function custom()
    {
        $data = request()->all();
        $response = Excel::download(new UsersExportPaid($data), 'runners_custom.xlsx');
        return $response;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function resources()
    {
        $data = Resource::all();
        return view('backend.resource.index',compact('data'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = Category::all();
        return view('backend.resource.create' , compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'title_ru' => 'required',
            'desc_ru' => 'required',
            'category_id' => 'required',
            'image' => 'required',
        ]);
        $resource = Resource::create([
            'online' => true,
            'category_id' => request('category_id'),
        ]);
        $image = $request->file('image');
        $filename = $resource->id .'.jpg';
		$path = public_path('uploads/runners/'. $filename);
        Image::make($image->getRealPath())->encode('jpg', 90)
        ->fit(400, 600)
        ->save($path);

        foreach (LaravelLocalization::getSupportedLocales() as $locale => $key) {
            if(request('title_'.$locale) !== null && request('desc_'.$locale) !== null){
                $resource->translateOrNew($locale)->title = request('title_'.$locale);
                $resource->translateOrNew($locale)->description = request('desc_'.$locale);
            }
        }
        $resource->save();

        return redirect()->action('PanelController@resources')->with('success','Успешно добавлено');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Resource::find($id);
        if(file_exists('uploads/runners/' . $id . '.jpg')){
             $delete = Image::make('uploads/runners/' . $id . '.jpg');
            $delete->destroy();
        }
        $data->delete();

        return redirect()->back()->with('success','Успешно удален');
    }
    public function pages()
    {
        $data = Pages::all();
        return view('backend.pages.index', compact('data'));
    }
    public function updatePage(Request $request, $id)
    {
        Pages::find($id)->update([
            'text'=> $request->text
        ]);
        return redirect()->action('PanelController@pages')->with('success','Успешно добавлено');
    }
    public function editPage($id)
    {
        $data = Pages::findOrFail($id);
        return view('backend.pages.edit', compact('data'));
    }
}
