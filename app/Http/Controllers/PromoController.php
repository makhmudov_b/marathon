<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Promo;
class PromoController extends Controller
{
    public function index()
    {
        $data = Promo::all();
        return view('backend.promo.index',compact('data'));
    }
    public function create()
    {
        return view('backend.promo.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'name' => 'required|unique:promos',
        ]);
        Promo::create([
            'name'=> request('name')
        ]);
        return redirect()->action('PromoController@index')->with('success','Успешно добавлено');
    }
}
