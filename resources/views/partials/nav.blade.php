<nav class="nav">
    <div class="container">
        <div class="col-md-9">
            @if(request()->is('*register*') || request()->is('*about*') || request()->is('*information*') || request()->is('*program*')
            || request()->is('*runners*') || request()->is('*summary*') || request()->is('*contacts*')
            )
            <a href="{{action('PageController@index')}}" class="nav__logo">
                Samarkand <br>
                Half Marathon
            </a>
            @endif
            <div class="nav__links">
                <a href="{{ action('PageController@about')  }}">@lang('main.nav.1')</a>
                <a href="{{ action('PageController@information')  }}">@lang('main.nav.2')</a>
                <a href="{{ action('PageController@program_first')  }}">@lang('main.nav.3')</a>
                <a href="{{ action('PageController@runners')  }}">@lang('main.nav.4')</a>
{{--                <a href="{{ action('PageController@summary')  }}">@lang('main.nav.5')</a>--}}
                <a href="{{ action('PageController@contacts') }}">@lang('main.nav.6')</a>
            </div>
        </div>
        <div class="col-md-3 text-right">
            <div class="nav__profile">
                @if(!Auth::check())
                <button id="profile">
                    <img src="{{ asset('img/profile.svg')  }}" width="40">
                </button>
                @else
                    @if(Auth::user()->is_paid)
                        <a href="{{action('PageController@profile')}}">
                            <img src="{{ asset('img/profile-green.svg')  }}" width="40">
                        </a>
                    @else
                    <a href="{{action('PageController@payment')}}">
                        <img src="{{ asset('img/profile-green.svg')  }}" width="40">
                    </a>
                    @endif
                @endif
            <div class="auth">
                <div class="col-md-6">
                    <div class="auth__title">
                        @lang('main.profile.cabinet')
                    </div>
                </div>
                <div class="col-md-6 text-right">
                    <img src="{{ asset('img/profile-green.svg') }}" alt="">
                </div>
                <div class="col-md-12">
                    <form action="{{ route('frontier') }}" method="POST">
                        @csrf
                        <label for="email">E-mail</label>
                        <input type="email" id="email" name="email">
                        <label for="password">@lang('main.registration.password')</label>
                        <input type="password" id="password" name="password">
                        <a href="{{ route('password.request') }}">@lang('main.forget')</a>
                        <br>
                        <button class="content__btn">@lang('main.login')</button>
                    </form>
                </div>
            </div>
        </div>
        <div class="nav__lang">
            <a href="{{ LaravelLocalization::getLocalizedURL('uz') }}">Uz</a>
            <a href="{{ LaravelLocalization::getLocalizedURL('ru') }}">Ru</a>
            <a href="{{ LaravelLocalization::getLocalizedURL('en') }}">En</a>
        </div>
    </div>
    <div class="md-float-right">
        <div class="hamburger toggle">
            <div class="hamburger__container">
              <div class="hamburger__inner"></div>
              <div class="hamburger__hidden"></div>
            </div>
        </div>
    </div>
    </div>
</nav>
<div class="auth-modal">
</div>
