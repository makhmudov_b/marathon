<section class="anim main animated none mob">
<div class="filter animation" style="animation-delay: 0.3s;">
    <div class="filter__title">
        Стоимость блюд
    </div>
    <div class="filter__info">
        до <span class="fitler__price">480 000</span> сум
    </div>
    <div class="filter__range range-slider">
        <input type="text" class="js-range-slider" value="" />
    </div>
    <div class="filter__title">
        Время приготовления
    </div>
    <div class="filter__info">
        до <span class="filter__time">40</span> минут
    </div>
    <div class="filter__range range-slider">
        <input type="text" class="js-range-slider-2" value="" />
    </div>
</div>
<div class="description-choice animation" style="animation-delay: 0.5s;">
    <div class="container">
        <div class="description-choice__title">
            Категория
        </div>
        <div class="description-choice__type">
            <input type="radio" name="size" id="size_1" value="small" checked />
            <label for="size_1">Все</label>

            <input type="radio" name="size" id="size_2" value="small" />
            <label for="size_2">Национальная</label>

            <input type="radio" name="size" id="size_3" value="small" />
            <label for="size_3">Европейская</label>

            <input type="radio" name="size" id="size_4" value="small" />
            <label for="size_4">Азиатская</label>

            <input type="radio" name="size" id="size_5" value="small" />
            <label for="size_5">Пицца</label>

            <input type="radio" name="size" id="size_6" value="small" />
            <label for="size_6">Бургеры</label>
        </div>
        <div class="description-choice__line"></div>
        <div class="description-choice__title">
            Бесплатная доставка
        </div>
        <div class="description-choice__type">
            <input type="radio" name="shipping" id="ship_1" value="small" checked />
            <label for="ship_1">да</label>
            <input type="radio" name="shipping" id="ship_2" value="small" />
            <label for="ship_2">не важно</label>
        </div>
    </div>
    <div class="description-choice__bottom">
        Применить фильтры
    </div>
</div>
</section>
