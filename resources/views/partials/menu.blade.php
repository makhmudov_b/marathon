<div class="menu">
        <p class="nav__logo">
            Samarkand <br>
            Half Marathon
        </p>
      <div class="hamburger is-active">
        <div class="hamburger__container">
          <div class="hamburger__inner"></div>
          <div class="hamburger__hidden"></div>
        </div>
      </div>
    <div class="menu__profile">
    @if(!Auth::check())
    <img src="{{ asset('img/profile.svg')  }}" width="40">
    <a href="{{action('PageController@login')}}">
        @lang('main.login')
    </a>
    @else
        @if(Auth::user()->is_paid)
            <img src="{{ asset('img/profile-green.svg')  }}" width="40">
            <a href="{{action('PageController@profile')}}">
                @lang('main.profile.cabinet')
            </a>
        @else
        <img src="{{ asset('img/profile-green.svg')  }}" width="40">
        <a href="{{action('PageController@payment')}}">
            @lang('main.profile.cabinet')
        </a>
        @endif
    @endif
    </div>
    <ul class="menu__links">
        <li><a href="{{ action('PageController@about')  }}">@lang('main.nav.1')</a></li>
        <li><a href="{{ action('PageController@information')  }}">@lang('main.nav.2')</a></li>
        <li><a href="{{ action('PageController@program_first')  }}">@lang('main.nav.3')</a></li>
        <li><a href="{{ action('PageController@runners')  }}">@lang('main.nav.4')</a></li>
{{--        <li><a href="{{ action('PageController@summary')  }}">@lang('main.nav.5')</a></li>--}}
        <li><a href="{{ action('PageController@contacts') }}">@lang('main.nav.6')</a></li>
    </ul>
    <div class="menu__lang">
        <a href="{{ LaravelLocalization::getLocalizedURL('uz') }}">Uz</a>
        <a href="{{ LaravelLocalization::getLocalizedURL('ru') }}">Ru</a>
        <a href="{{ LaravelLocalization::getLocalizedURL('en') }}">En</a>
    </div>
</div>
<div class="modal">

</div>
