<div class="content__social">
    <span class="social__text">@lang('main.content.social')</span>
    <a href="#"><img src="{{ asset('img/face.svg')  }}" /></a>
    <a href="#"><img src="{{ asset('img/insta.svg')  }}" /></a>
    <a href="#"><img src="{{ asset('img/tele.svg')  }}" /></a>
</div>