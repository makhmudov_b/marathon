<section class="useful desktop">
    <div class="container">
        <h2 class="useful__h2 text-center">А в приложении все еще удобнее</h2>
        <div class="row">
            <div class="col-md-4 col-sm-6 index text-center">
                <button class="useful__btn"><img src="{{ asset('/img/playmarket.svg') }}">Загрузить для Android</button>
            </div>
            <div class="col-md-4 hidden-sm"></div>
            <div class="col-md-4 col-sm-6 index text-center">
                <button class="useful__btn"><img src="{{ asset('/img/appstore.svg') }}">Загрузить для ApPlE</button>
            </div>
        </div>
    </div>
    <img src="{{ asset('/img/useful.png') }}" class="useful__img useful__img-2 desktop">
    <img src="{{ asset('/img/useful1.png') }}" class="useful__img useful__img-1 desktop">
</section>