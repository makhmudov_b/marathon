@extends('layouts.frontend')
@section('content')
<div class="contacts">
<div class="contacts__title">@lang('main.content.contacts')</div>
<div class="contacts__wrapper">
    <div class="container text-center">
        <div class="col-lg-12">
            <div class="contacts__info">
                @lang('main.contacts.1'):
            </div>
            <div class="contacts__number">
                +998 71 207-40-10
            </div>
            <div class="contacts__bottom">
                <div class="left">
                    halfmarathon@acdf.uz
                </div>
                <div class="right">
                    @lang('main.contacts.2')
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="contacts__info">
            @lang('main.contacts.3'):
            </div>
            <div class="contacts__number">
                +998 99 363-22-36
            </div>
            <div class="contacts__bottom">
                <div class="left">
                    halfmarathon@acdf.uz
                </div>
                <div class="right">
                    @lang('main.contacts.4')
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="contacts__info">
                @lang('main.contacts.5')
            </div>
            <div class="contacts__number">
                +998 71 207-40-10
            </div>
            <div class="contacts__bottom">
                <div class="left">
                    f.maksudova@acdf.uz
                </div>
                <div class="right">
                    <a href="https://forms.gle/CYQvvazKFhN5HbHN8" target="_blank">
                        @lang('main.contacts.6')
                    </a>
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="contacts__info">
                @lang('main.contacts.7'):
            </div>
            <div class="contacts__number">
                +998 71 207-40-50
            </div>
            <div class="contacts__bottom">
                <div class="left">
                    halfmarathon@acdf.uz
                </div>
                <div class="right">
                    @lang('main.contacts.8')
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="contacts__info">
                @lang('main.contacts.9')
            </div>
            <div class="contacts__number">
                +998 71 207-40-10
            </div>
            <div class="contacts__bottom">
                <div class="left">
                    m.badalova@acdf.uz
                </div>
                <div class="right">
                    @lang('main.contacts.10')
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
