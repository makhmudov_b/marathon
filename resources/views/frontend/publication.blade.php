@extends('layouts.frontend')
@section('content')
<div class="inner">
    <div class="container text-center">
        <div class="content__title">@lang('main.content.interesting')</div>
        <div class="inner__wrapper">
            <div class="col-md-2">
                <div class="inner__wrapper--date">
                    <b>22</b>
                    <p>Июля, 2019</p>
                </div>
            </div>
            <div class="col-md-10">
                <div class="inner__wrapper--text">
                    Инструмент маркетинга, вопреки мнению П.Друкера, спонтанно специфицирует медиамикс
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="inner__info">
            <div class="col-md-5 text-right">
                <div class="borders-big">
                    <img src="{{ asset('/img/info1.png')  }}" class="img"/>
                </div>
                @include('partials.socails')
            </div>
            <div class="col-md-7 text-left">
                <div class="inner__info--title">
                    Опрос, как следует из вышесказанного, консолидирует conversion rate, осознавая социальную ответственность бизнеса
                </div>
                <div class="inner__info--text">
                    Инструмент маркетинга, вопреки мнению П.Друкера, спонтанно специфицирует медиамикс. Управление брендом непосредственно раскручивает план размещения, осознав маркетинг как часть производства. Опрос, как следует из вышесказанного, консолидирует conversion rate, осознавая социальную ответственность бизнеса.
                    <br><br>
                    Эволюция мерчандайзинга все еще интересна для многих. Корпоративная культура, анализируя результаты рекламной кампании, охватывает институциональный PR. Селекция бренда упорядочивает пресс-клиппинг, опираясь на опыт западных коллег. Стимулирование сбыта отражает формат события.
                    <br><br>
                    Рекламный бриф создает пресс-клиппинг, учитывая современные тенденции. Контент, безусловно, поддерживает креативный рейтинг. Как отмечает Майкл Мескон, поисковая реклама упорядочивает конвергентный product placement.
                    <br><br>
                    Медийный канал повсеместно программирует конвергентный медиамикс. Поэтому креатив специфицирует департамент маркетинга и продаж
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="content__title">@lang('main.content.also')</div>
        @include('partials.interesting')
        @include('partials.socails')
    </div>
</div>
@endsection