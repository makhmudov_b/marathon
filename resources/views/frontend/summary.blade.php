@extends('layouts.frontend')
@section('content')
<div class="contacts__title">
        @lang('main.nav.1')
</div>
<div class="about--info">
<div class="container">
<div class="about__summary">
    <div class="about__summary-distance">
        Дистанция:
        <input type="radio" id="distance1" name="distance" value="21" checked>
        <label for="distance1">21km</label>
        <input type="radio" id="distance2" name="distance" value="10">
        <label for="distance2">10km</label>
        <input type="radio" id="distance3" name="distance" value="3">
        <label for="distance3">3km</label>
    </div>
    <div class="about__summary-gender">
        Пол:
        <input type="radio" id="gender1" name="gender" value="0" checked>
        <label for="gender1">Мужской</label>
        <input type="radio" id="gender2" name="gender" value="1">
        <label for="gender2">Женский</label>
    </div>
    <div class="about__summary-number">
        Стартовый номер:
        <input type="text">
    </div>
    </div>
    <table class="about__table">
    <tr>
        <td>Место</td>
        <td>Участник</td>
        <td>Страна</td>
        <td>Город</td>
        <td>Номер</td>
        <td>Финиш</td>
        <td>Чип-время</td>
    </tr>
    @for($i = 1; $i<11;$i ++)
    <tr>
        <td>{{$i}}</td>
        <td>Антон Хатамов</td>
        <td>Узбекистан</td>
        <td>Самарканд</td>
        <td>87</td>
        <td>00:07:52</td>
        <td>00:07:52</td>
    </tr>
    @endfor
    </table>
    <div class="text-center">
        <a class="main__register">
            Показать еще
        </a>
    </div>
</div>
</div>
@endsection
