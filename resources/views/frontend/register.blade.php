@extends('layouts.frontend')
@section('content')
<div class="text-center">
        <div class="register__title">@lang('main.register')</div>
        <div class="register">
            <div class="container">
                <div class="col-md-9">
                    <div class="whitespace">
                        <div class="register__status">
                            <div class="register__status--wrapper text-left active">
                                1. @lang('main.registration.1')
                            </div>
                            <div class="register__status--wrapper">
                                2. @lang('main.registration.2')
                            </div>
                            <div class="register__status--wrapper text-right">
                                3. @lang('main.registration.3')
                            </div>
                        </div>
                        <form action="{{ action('AuthController@register')  }}" autocomplete="off" method="POST" class="register__form text-left">
                            @csrf
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="register__form--raw">
                                        <label class="title" for="email">E-mail</label>
                                        <input required="required" type="email" name="email" value="{{ old('email') }}" id="email">
                                        @if($errors->has('email'))
                                            <div class="error">*@lang('main.registration.email')</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="register__form--raw">
                                        <label class="title" for="tel">@lang('main.registration.phone')</label>
                                        <input required="required" type="tel" maxlength="18" name="phone" value="{{ old('phone') }}" id="tel">
                                        @if($errors->has('phone'))
                                            <div class="error">*@lang('main.registration.phoneror')</div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="register__form--raw">
                                        <label class="title" for="name">@lang('main.registration.name')</label>
                                        <input required="required" class="latin" type="text" name="name" value="{{ old('name') }}" id="name">
                                        @if($errors->has('name'))
                                            <div class="error">*@lang('main.registration.wrong')</div>
                                        @endif
                                    </div>
                                </div>
                            <div class="col-md-6">
                                <div class="register__form--raw">
                                    <label class="title" for="last_name">@lang('main.registration.lastname')</label>
                                    <input required="required" class="latin" type="text" name="last_name" value="{{ old('last_name') }}" id="lastname">
                                    @if($errors->has('last_name'))
                                        <div class="error">*@lang('main.registration.wrong')</div>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="register__form--raw">
                                    <label class="title" for="birthday">@lang('main.registration.birthday')</label>
                                    <input required="required" type="date" name="birthday" value="{{ old('birthday') }}" id="birthday">
                                    @if($errors->has('birthday'))
                                        <div class="error">*@lang('main.registration.wrong')</div>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="register__form--raw">
                                    <label class="title">@lang('main.registration.gender')</label>
                                    <input type="radio" name="gender" value="0" id="gender1" @if(old('gender') == '0') checked="checked" @endif required>
                                    <label for="gender1">@lang('main.registration.male')</label>
                                    <input type="radio" name="gender" value="1" id="gender2" @if(old('gender') == '1') checked="checked" @endif>
                                    <label for="gender2">@lang('main.registration.female')</label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="register__form--raw">
                                    <label class="title" for="country">@lang('main.registration.country')</label>
                                    <select name="country" class="crs-country" id="country" data-show-default-option="false" data-region-id="region">
                                        @if($errors->has('country'))
                                            <div class="error">*@lang('main.registration.wrong')</div>
                                        @endif
                                    </select>
                                    <select id="region" class="display-none"></select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="register__form--raw">
                                    <label class="title" for="city">@lang('main.registration.city')</label>
                                    <input type="text" name="city" id="city" required="required" value="{{ old('city') }}">
                                    @if($errors->has('city'))
                                        <div class="error">*@lang('main.registration.wrong')</div>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="register__form--raw">
                                    <label class="title" for="contact">@lang('main.registration.extra')</label>
                                    <input required="required" type="text" maxlength="18" name="extra_phone" value="{{ old('extra_phone') }}" id="contact">
                                    @if($errors->has('extra_phone'))
                                        <div class="error">*@lang('main.registration.wrong')</div>
                                    @endif
                                </div>
                                <div class="info">
                                    @lang('main.registration.info')
                                </div>
                            </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="register__form--raw">
                                        <label class="title" for="size">@lang('main.registration.size')</label>
                                        <select name="size" id="size">
                                            <option value="XXL" {{ old('size') == 'XXL' ? 'selected' : '' }}>XXL</option>
                                            <option value="XL" {{ old('size') == 'XL' ? 'selected' : '' }}>XL</option>
                                            <option value="L" {{ old('size') == 'L' ? 'selected' : '' }}>L</option>
                                            <option value="M" {{ old('size') == 'M' ? 'selected' : '' }}>M</option>
                                            <option value="S" {{ old('size') == 'S' ? 'selected' : '' }}>S</option>
                                            <option value="XS" {{ old('size') == 'XS' ? 'selected' : '' }}>XS</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="register__form--raw">
                                        <label class="title" for="promo">@lang('main.registration.promo')</label>
                                        <input type="text" id="promo" name="promo" value="{{ old('promo') }}">
                                        @if($errors->has('promo'))
                                            <div class="error">*@lang('main.registration.wrong')</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="register__form--raw">
                                        <label class="title" for="password">@lang('main.registration.password')</label>
                                        <input type="password" id="password" required name="password" value="{{ old('password') }}">
                                        @if($errors->has('password'))
                                            <div class="error">*@lang('main.registration.wrong')</div>
                                        @endif
                                    </div>
                                <div class="info">
                                    @lang('main.registration.warn')
                                </div>
                                </div>
                            <div class="col-md-12">
                                <div class="register__distance">@lang('main.registration.distance')</div>
                                <div class="distance" id="getter">
                                    @foreach($data as  $key => $datas)
                                    <input type="radio" name="distance_id" @if(old('distance_id') == $datas->id) checked @endif  @if(old('distance_id', null) == null) @if($key === 0) checked @endif @endif value="{{$datas->id}}"
                                           data-place="{{$datas->place}}"
                                           data-date="{{\Carbon\Carbon::parse($datas->date)->locale(app()->getLocale())->isoFormat('D MMMM')}}"
                                           data-time="{{$datas->time}}"
                                           data-long="{{$datas->long}}"
                                           data-price="{{app()->getLocale() == 'en' ? $datas->price_usd  : $datas->price}}"
                                           data-usd="{{$datas->price_usd}}"
                                           data-id="{{$datas->id}}"
                                           id="distance{{$datas->id}}"
                                           class="display-non" />
                                    <label for="distance{{$datas->id}}">
                                        <b class="register__distance--long">{{ $datas->long }}</b>
                                        <span class="register__distance--price">{{ app()->getLocale() == 'en' ? $datas->price_usd  : $datas->price }} @if(app()->getLocale() == 'en') $ @else @lang('main.registration.sum') @endif</span>
                                    </label>
                                    @endforeach
                                </div>
                            </div>
                            <div class="col-md-12" id="level">
                                    <label class="title" for="level">@lang('main.registration.distance')</label>
                                    <select name="level" id="level">
                                        <option value="A" @if(old('level') == 'A' && old('distance_id') == '1') selected @endif class="amateur">@lang('main.registration.a1')</option>
                                        <option value="B" @if(old('level') == 'B' && old('distance_id') == '1') selected @endif class="amateur">@lang('main.registration.b1')</option>
                                        <option value="C" @if(old('level') == 'C' && old('distance_id') == '1') selected @endif class="amateur">@lang('main.registration.c1')</option>

                                        <option value="A" @if(old('level') == 'A' && old('distance_id') == '2') selected @endif class="pro">@lang('main.registration.a2')</option>
                                        <option value="B" @if(old('level') == 'B' && old('distance_id') == '2') selected @endif class="pro">@lang('main.registration.b2')</option>
                                        <option value="C" @if(old('level') == 'C' && old('distance_id') == '2') selected @endif class="pro">@lang('main.registration.c2')</option>
                                    </select>
                            </div>
                            </div>
                                <div class="agreement">
                                    <input type="checkbox" id="invalid" value="1" name="invalid">
                                    <label for="invalid" class="information-big">@lang('main.registration.invalid')</label>
                                    <p class="information">@lang('main.registration.check')</p>
                                    <br>
                                        <input type="checkbox" id="agreement" required="required">
                                    <label class="information-big" for="agreement">
                                    @lang('main.registration.agreement')
                                    </label>
                                    <br><br>
                                    <input type="checkbox" id="apply" name="apply" required="required">
                                    <label for="apply" class="information-big">
                                        @lang('main.registration.agree1')
                                        <a href="http://samarqand21.uz/Публичный договор.docx">
                                        @lang('main.registration.agree2')
                                        </a>
                                    </label>
                                </div>
                                <button class="main__register mb-30">
                                    @lang('main.registration.save')
                                </button>
                        </form>
                    <div class="clearfix"></div>
                    </div>
                </div>
                <div class="col-md-3 hidden-sm">
                    <div class="whitespace-right">
                        <div class="register__right pl-30">
                            @lang('main.registration.pay')
                        </div>
                        <div class="register__price pl-30" id="main_price">
                            @if(app()->getLocale() == 'en')
                                {{$data[0]->price_usd.' $'}}
                            @else
                                {{ $data[0]->price }} @lang('main.registration.sum')
                            @endif
                        </div>
                        <div class="register__payment">
                            <div class="register__payment--text pl-30">
                                @lang('main.registration.method'):
                            </div>
                            <div class="flexer">
                                <img src="{{ asset('/img/visa.svg')  }}" />
                                <img src="{{ asset('/img/pay1.svg')  }}" />
                                <img src="{{ asset('/img/pay2.svg')  }}" />
                                <img src="{{ asset('/img/pay3.svg')  }}" />
                                <img src="{{ asset('/img/pay4.svg')  }}" />
                            </div>
                        </div>
                        <div class="register__right pl-30">
                            @lang('main.registration.about')
                        </div>
                        <div class="register__info pl-30">
                            <div class="left">@lang('main.registration.distances'):</div>
                            <div class="right" id="main_long">{{$data[0]->long}}</div>
                        </div>
                        <div class="register__info pl-30">
                            <div class="left">@lang('main.registration.start'):</div>
                            <div class="right" id="main_date">{{\Carbon\Carbon::parse($data[0]->date)->locale(app()->getLocale())->isoFormat('D MMMM')}}</div>
                        </div>
                        <div class="register__info pl-30">
                            <div class="left">@lang('main.registration.place'):</div>
                            <div class="right" id="main_place">{{app()->getLocale() == 'ru' ? $data[0]->place : (app()->getLocale() == 'en' ? $data[0]->place_en : $data[0]->place_uz)}}</div>
                        </div>
                        <div class="register__info pl-30">
                            <div class="left">@lang('main.registration.times'):</div>
                            <div class="right" id="main_time">{{$data[0]->time}}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>
@endsection
@section('script')
    <script src="{{asset('/js/jquery.crs.min.js')}}"></script>
    <script src="{{asset('/js/intlTelInput-jquery.min.js')}}"></script>
    <script>
    @if(old('country', null) != null)
    $(document).ready(function() {
      $('#country option[value="{{ old('country') }}"]').prop("selected", true).parent().trigger('change');
    });
    @endif
    $('.latin').bind('keyup',function(){
        $(this).val($(this).val().replace(/[^a-z ]/i, ""))
    });
      $('#tel').intlTelInput({
            nationalMode: false,
            utilsScript: "{{asset('js/utils.js')}}",
            preferredCountries: [ "uz" ],
      });
      $('#contact').intlTelInput({
            nationalMode: false,
            utilsScript: "{{asset('js/utils.js')}}",
            preferredCountries: [ "uz" ],
      });
      $(document).ready(function(){
          changeLabel(1);
      });
      let distance = $('#getter');
      let mainPrice = $('#main_price');
      let mainLong = $('#main_long');
      let mainTime = $('#main_time');
      let mainDate = $('#main_date');
      let mainPlace = $('#main_place');
      let amateur = $('.amateur');
      let pro = $('.pro');
      function change(){
        let price = distance.children('input:checked').data('price');
        let long = distance.children('input:checked').data('long');
        let time = distance.children('input:checked').data('time');
        let date = distance.children('input:checked').data('date');
        let place = distance.children('input:checked').data('place');

        let show = distance.children('input:checked').data('id');
        changeLabel(show);
        let value =  '{{ app()->getLocale() == 'en' ? '$'  : ' UZS'}}';
        mainPrice.html(price + value);
        mainLong.html(long);
        mainTime.html(time);
        mainDate.html(date);
        // mainPlace.html(place);
        return true;
      };
      function changeLabel(show){
          if (show === 3) {
              pro.prop('disabled', true);
              pro.hide();
              pro.removeAttr('selected');

              amateur.show();
              amateur.prop('disabled', false);
              amateur.first().attr('selected', 'selected');
              $('#level').addClass('opacity-hide');
          }
          if (show === 2) {
              pro.prop('disabled', true);
              pro.hide();
              pro.removeAttr('selected');

              amateur.show();
              amateur.prop('disabled', false);
              amateur.first().attr('selected', 'selected');
                $('#level').removeClass('opacity-hide');
          }
          if (show === 1) {
              amateur.prop('disabled', true);
              amateur.hide();
              amateur.removeAttr('selected');

              pro.show();
              pro.prop('disabled', false);
              pro.first().attr('selected', 'selected');
            $('#level').removeClass('opacity-hide');
          }
      }
      distance.children('input').on('click',function(){
          change();
      });
    </script>
@endsection
