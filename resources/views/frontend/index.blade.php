@extends('layouts.frontend')
@section('content')
<div class="main">
    <div class="container">
        <div class="main__title">
            <img src="{{asset('img/logo.svg')}}" alt="">
        </div>
        @if(!Auth::check())
        <a href="{{ action('PageController@register') }}" class="main__register mt-50">
            @lang('main.register_now')
        </a>
        @else
            @if(Auth::user()->is_paid)
                <a href="{{ action('PageController@profile') }}" class="main__register">
                    @lang('main.profile.cabinet')
                </a>
            @else
                <a href="{{ action('PageController@payment') }}" class="main__register">
                    @lang('main.profile.cabinet')
                </a>
            @endif
        @endif
    </div>
</div>
<div class="content">
    <div class="container">
        <div class="content__title">@lang('main.content.title')</div>
        <div class="content__timer">
            <div class="content__timer--padding">
                <span class="counter days" id="days">0</span>
                <p class="title">@lang('main.timer.days')</p>
            </div>
            <div class="content__timer--padding">
                <span class="counter hours" id="hours">0</span>
                <p class="title">@lang('main.timer.hours')</p>
            </div>
            <div class="content__timer--padding">
                <span class="counter minute" id="minute">0</span>
                <p class="title">@lang('main.timer.minute')</p>
            </div>
            <div class="content__timer--padding">
                <span class="counter second" id="second">0</span>
                <p class="title">@lang('main.timer.second')</p>
            </div>
        </div>
        <div class="content__title">@lang('main.content.runners')</div>
        <div class="content__slider">
            @foreach($runners as $runner)
            <div class="content__slider--1 content__slider--item borders">
                <div class="inside-image">
                    <img src="{{ asset('uploads/runners/'.$runner->id.'.jpg')  }}">
                </div>
                <div class="inside">
                    <b>{{$runner->getTranslation(app()->getLocale())->title}}</b>
                    <p>{{$runner->getTranslation(app()->getLocale())->description}}</p>
                </div>
            </div>
            @endforeach
        </div>
        </div>
            <div class="text-center">
                <a href="{{action('PageController@runners')}}" class="content__btn mb-70">
                    @lang('main.content.all')
                </a>
            </div>
        <div class="container">
            <div class="content__title">
                @lang('main.content.organizers')
            </div>
            <div class="content__partners">
                <a href="http://madaniyat.uz/" target="_blank" class="content__partners--block">
                    <img src="{{ asset('img/ministry.svg')  }}" style="height: 100px;" />
                </a>
                <a href="http://acdf.uz/" target="_blank" class="content__partners--block">
                    <img src="{{ asset('img/fond.svg')  }}" />
                </a>
                <a href="#" target="_blank" class="content__partners--block opacity-hide">
                    <img src="{{ asset('img/fond.svg')  }}" />
                </a>
            </div>
            <div class="content__title">
                @lang('main.content.general')
            </div>
            <div class="content__partners">
                <a href="https://uzbektourism.uz/" target="_blank" class="content__partners--block">
                    <img src="{{ asset('img/government.svg')  }}" />
                </a>
                <a href="https://samarkand.uz/" target="_blank" class="content__partners--block">
                    <img src="{{ asset('img/samarkand.svg')  }}" />
                </a>
                <a href="#" target="_blank" class="content__partners--block opacity-hide">
                    <img src="{{ asset('img/fond.svg')  }}" />
                </a>
            </div>
            <div class="content__title">
                @lang('main.content.fitness')
            </div>
            <div class="content__partners">
                <a href="https://russiarunning.com/" target="_blank" class="content__partners--block" style="width: 250px;">
                    <img src="{{ asset('img/partner7.svg')  }}" />
                </a>
                <a href="https://beatfilmfestival.ru" target="_blank" class="content__partners--block" style="width: 250px;">
                    <img src="{{ asset('img/partner4.svg')  }}" />
                </a>
                <a href="https://www.facebook.com/triathlonuz/" target="_blank" class="content__partners--block" style="width: 250px;">
                    <img src="{{ asset('img/partner2.svg')  }}" />
                </a>
                <a href="https://befit.uz/" target="_blank" class="content__partners--block" style="width: 250px;">
                    <img src="{{ asset('img/befit.svg')  }}" />
                </a>
                <a href="https://www.afisha.uz/" target="_blank" class="content__partners--block" style="width: 250px;">
                    <img src="{{ asset('img/partner3.svg')  }}" width="150" />
                </a>
                <a href="https://themag.uz/" target="_blank" class="content__partners--block" style="width: 250px;">
                    <img src="{{ asset('img/partner6.svg')  }}" />
                </a>
                <a href="http://www.oriatfm.uz/" target="_blank" class="content__partners--block" style="width: 250px;">
                    <img src="{{ asset('img/partner1.svg')  }}" />
                </a>
                <a href="https://mtrk.uz/uz/" target="_blank" class="content__partners--block" style="width: 250px;">
                    <img src="{{ asset('img/partner5.svg')  }}" />
                </a>
            </div>
            <div class="content__title">
                @lang('main.content.sponsor')
            </div>
            <div class="content__partners">
                <a href="https://nbu.uz/uz/" target="_blank" class="content__partners--block" style="width: 250px;">
                    <img src="{{ asset('img/logotype6.svg')}}">
                </a>
                <a href="http://www.railway.uz/ru/" target="_blank" class="content__partners--block" style="width: 250px;">
                    <img src="{{ asset('img/logotype10.svg')}}">
                </a>
                <a href="https://ums.uz/" target="_blank" class="content__partners--block" style="width: 250px;">
                    <img src="{{ asset('img/logotype13.svg')}}">
                </a>
                <a href="https://ucell.uz/" target="_blank" class="content__partners--block" style="width: 250px;">
                    <img src="{{ asset('img/logotype12.svg')}}">
                </a>
                <a href="https://uzpsb.uz/" target="_blank" class="content__partners--block" style="width: 250px;">
                    <img src="{{ asset('img/logotype9.svg')}}">
                </a>
                <a href="https://www.asakabank.uz/ru" target="_blank" class="content__partners--block" style="width: 250px;">
                    <img src="{{ asset('img/logotype15.svg')}}">
                </a>
                <a href="https://ofb.uz/uz/" target="_blank" class="content__partners--block" style="width: 250px;">
                    <img src="{{ asset('img/logotype7.svg')}}">
                </a>
                <a href="http://www.maxam-chirchiq.uz/ru/" target="_blank" class="content__partners--block" style="width: 250px;">
                    <img src="{{ asset('img/logotype5.svg')}}">
                </a>
                <a href="https://hamkorbank.uz/" target="_blank" class="content__partners--block" style="width: 250px;">
                    <img src="{{ asset('img/logotype1.svg')}}" width="150">
                </a>
                <a href="https://www.davrbank.uz/" target="_blank" class="content__partners--block" style="width: 250px;">
                    <img src="{{ asset('img/logotype14.svg')}}" width="200">
                </a>
                <a href="https://www.kapitalbank.uz/ru/" target="_blank" class="content__partners--block" style="width: 250px;">
                    <img src="{{ asset('img/logotype2.svg')}}">
                </a>
                <a href="https://turonbank.uz/" target="_blank" class="content__partners--block" style="width: 250px;">
                    <img src="{{ asset('img/logotype11.svg')}}">
                </a>
                <a href="https://makromarket.uz/" target="_blank" class="content__partners--block" style="width: 250px;">
                    <img src="{{ asset('img/logotype4.svg')}}">
                </a>
                <a href="https://fb.com/redtaguzbekistan" target="_blank" class="content__partners--block" style="width: 250px;">
                    <img src="{{ asset('img/logotype8.svg')}}">
                </a>
                <a href="https://fb.com/FLOShoesUzbekistan" target="_blank" class="content__partners--block" style="width: 250px;">
                    <img src="{{ asset('img/logotype3.svg')}}">
                </a>
                <a href="" target="_blank" class="content__partners--block opacity-hide" style="width: 250px;">
                    <img src="{{ asset('img/logotype15.svg')}}">
                </a>
            </div>
        </div>
</div>
@endsection
@section('script')
        <script>
var startDay= new Date("2019/11/03 09:00:00");
var now = new Date();
var upgradeTime = (startDay.getTime() - now.getTime()) / 1000;

var seconds = upgradeTime;
    function timer() {
      var days        = Math.floor(seconds/24/60/60);
      var hoursLeft   = Math.floor((seconds) - (days*86400));
      var hours       = Math.floor(hoursLeft/3600);
      var minutesLeft = Math.floor((hoursLeft) - (hours*3600));
      var minutes     = Math.floor(minutesLeft / 60);
      var remainingSeconds = parseInt(seconds % 60);
      function pad(n) {
        return (n < 10 ? "0" + n : n);
      }
      document.getElementById('days').innerHTML = pad(days);
      document.getElementById('hours').innerHTML = pad(hours);
      document.getElementById('minute').innerHTML = pad(minutes);
      document.getElementById('second').innerHTML = pad(remainingSeconds);
      if (seconds === 0) {
        clearInterval(countdownTimer);
      } else {
        seconds--;
      }
    }
if(startDay > now){
    setInterval('timer()', 1000);
}
$(".content__slider").fadeOut();
$(document).ready(function(){
    $(".content__slider").fadeIn();
});
$(".content__slider").slick({
    infinite: true,
    dots: false,
    slidesToShow: 3,
    slidesToScroll: 1,
  responsive: [{
      breakpoint: 1200,
      settings: {
        slidesToShow: 3,
      }
    },
    {
      breakpoint: 992,
      settings: {
        slidesToShow: 2,
      }
    },
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 2,
      }
    },
    {
      breakpoint: 500,
      settings: {
        slidesToShow: 1,
      }
    }]
});
</script>
@endsection
