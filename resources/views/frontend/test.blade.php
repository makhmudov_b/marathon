@extends('layouts.frontend')
@section('content')
<div class="contacts__title">
    @lang('main.nav.2')
</div>
<div class="about--info">
    <div class="about__information">
        <div class="container">
            <div class="col-md-3">
                <ul id="getter">
                <a href="#main" data-hash="main" class="anchor">@lang('main.information.1')</a>
                <a href="#register" data-hash="register" class="anchor">@lang('main.information.2')</a>
                <a href="#number" data-hash="number" class="anchor">@lang('main.information.3')</a>
                <a href="#competition" data-hash="competition" class="anchor">@lang('main.information.4')</a>
                <a href="#visa" data-hash="visa" class="anchor">@lang('main.information.5')</a>
                </ul>
            </div>
            <div class="col-md-9">
                {!! $data->text !!}
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
$(".anchor").click(function(e) {
    e.preventDefault();
    let aid = $(this).attr("href");
    $('html,body').animate({scrollTop: $(aid).offset().top},'slow');
    let hash = $(this).data('hash');
    $('#getter').children('a').removeClass('active');
    $('a[data-hash='+hash+']').addClass('active');
});
$(window).scroll(function () {
var scrollTop     = $(window).scrollTop(),
    elementOffset = $('.about--info-right').offset().top;
    console.log(scrollTop);
    if(scrollTop > elementOffset && scrollTop < 8800 ){
        $('#getter').addClass('pos-fixed')
    }
    else{
        $('#getter').removeClass('pos-fixed')
    }
})

</script>
@endsection
