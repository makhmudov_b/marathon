@extends('layouts.frontend')
@section('content')
<div class="inner">
    <div class="container text-center">
        <div class="content__title">@lang('main.content.interesting')</div>
        <div class="inner__wrapper">
            <div class="info__interesting">
                <div class="col-md-4 text-left">
                    <div class="borders-big">
                        <img src="{{ asset('/img/info1.png')  }}" class="img"/>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="info__interesting--title">
                        Реклама определяет рейтинг, повышая конкуренцию
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="info__interesting--text">
                    Реклама определяет рейтинг, повышая конкуренцию. Построение бренда интуитивно переворачивает обществвенный диктат потребителя. Как отмечает Майкл Мескон, поисковая реклама упорядочивает конвергентный product placement.
                    <p class="date">
                        22 июля, 2019
                    </p>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="info__interesting">
                <div class="col-md-4">
                    <div class="info__interesting--title">
                        Реклама определяет рейтинг, повышая конкуренцию
                    </div>
                </div>
                <div class="col-md-4 text-left">
                    <div class="borders-big">
                        <img src="{{ asset('/img/info1.png')  }}" class="img"/>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="info__interesting--text">
                    Реклама определяет рейтинг, повышая конкуренцию. Построение бренда интуитивно переворачивает обществвенный диктат потребителя. Как отмечает Майкл Мескон, поисковая реклама упорядочивает конвергентный product placement.
                    <p class="date">
                        22 июля, 2019
                    </p>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="info__interesting">
                <div class="col-md-4 text-left">
                    <div class="borders-big">
                        <img src="{{ asset('/img/info1.png')  }}" class="img"/>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="info__interesting--title">
                        Реклама определяет рейтинг, повышая конкуренцию
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="info__interesting--text">
                    Реклама определяет рейтинг, повышая конкуренцию. Построение бренда интуитивно переворачивает обществвенный диктат потребителя. Как отмечает Майкл Мескон, поисковая реклама упорядочивает конвергентный product placement.
                    <p class="date">
                        22 июля, 2019
                    </p>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="info__interesting">
                <div class="col-md-4">
                    <div class="info__interesting--title">
                        Реклама определяет рейтинг, повышая конкуренцию
                    </div>
                </div>
                <div class="col-md-4 text-left">
                    <div class="borders-big">
                        <img src="{{ asset('/img/info1.png')  }}" class="img"/>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="info__interesting--text">
                    Реклама определяет рейтинг, повышая конкуренцию. Построение бренда интуитивно переворачивает обществвенный диктат потребителя. Как отмечает Майкл Мескон, поисковая реклама упорядочивает конвергентный product placement.
                    <p class="date">
                        22 июля, 2019
                    </p>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            </div>
        </div>
        @include('partials.socails')
    </div>
</div>
@endsection
