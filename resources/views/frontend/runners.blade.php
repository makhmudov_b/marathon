@extends('layouts.frontend')
@section('content')
<div class="text-center">
<div class="contacts__title">@lang('main.content.runners')</div>
<div class="about__runners">
    <div class="about__wrapper mt-120">
            @foreach($runners as $runner)
            <div class="content__slider--item mb-70">
                <div class="inside-image">
                    <img src="{{ asset('uploads/runners/'.$runner->id.'.jpg')  }}">
                </div>
                <div class="inside">
                    <b>{{$runner->getTranslation(app()->getLocale())->title}}</b>
                    <p>{{$runner->getTranslation(app()->getLocale())->description}}</p>
                </div>
            </div>
            @endforeach
    </div>
    </div>
</div>
@endsection
