@extends('layouts.frontend')
@section('content')
<div class="text-center">
        <div class="register__title">@lang('main.profile.cabinet')</div>
        <div class="register">
            <div class="container">
                <div class="row">
                <div class="col-md-9">
                    <div class="whitespace">
                        <form action="{{ action('AuthController@update')  }}" autocomplete="off" method="POST" class="register__form text-left">
                            @csrf
                            <div class="col-md-12">
                                <br><br><br>
                            <div class="about--info-right">
                                <h3 id="main">
                                    @lang('main.profiles.main')
                                </h3>
                                <div class="about__information--title">
                                    @lang('main.profiles.personal')
                                </div>
                            </div>
                            </div>
                                <div class="col-md-6">
                                    <div class="register__form--raw">
                                        <label class="title bold" for="email">E-mail</label>
                                        <input type="text" value="{{$data->email}}" name="email" required="required">
                                        @if($errors->has('email'))
                                            <div class="error">{{$errors->first('email')}}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="register__form--raw">
                                        <label class="title bold" for="tel">@lang('main.registration.phone')</label>
                                        <input type="text" value="{{$data->phone}}" name="phone" id="tel" required="required">
                                        @if($errors->has('phone'))
                                            <div class="error">{{$errors->first('phone')}}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="register__form--raw">
                                        <label class="title bold" for="name">@lang('main.registration.name')</label>
                                        <input type="text" value="{{$data->name}}" class="latin" name="name" required="required">
                                        @if($errors->has('name'))
                                            <div class="error">{{$errors->first('named')}}</div>
                                        @endif
                                    </div>
                                </div>
                            <div class="col-md-6">
                                <div class="register__form--raw">
                                    <label class="title bold" for="last_name">@lang('main.registration.lastname')</label>
                                    <input type="text" value="{{$data->last_name}}" class="latin" name="last_name" required="required">
                                        @if($errors->has('last_name'))
                                            <div class="error">{{$errors->first('last_name')}}</div>
                                        @endif
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="register__form--raw">
                                    <label class="title" for="birthday">@lang('main.registration.birthday')</label>
                                    <input required="required" type="date" name="birthday" value="{{ $data->birthday }}" id="birthday">
                                    @if($errors->has('birthday'))
                                        <div class="error">*@lang('main.registration.wrong')</div>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="register__form--raw">
                                    <label class="title">@lang('main.registration.gender')</label>
                                    <input type="radio" name="gender" value="0" id="gender1" @if($data->gender == '0') checked="checked" @endif>
                                    <label for="gender1">@lang('main.registration.male')</label>
                                    <input type="radio" name="gender" value="1" id="gender2" @if($data->gender == '1') checked="checked" @endif>
                                    <label for="gender2">@lang('main.registration.female')</label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="register__form--raw">
                                    <label class="title bold" for="country">@lang('main.registration.country')</label>
                                    <select name="country" class="crs-country" id="country" data-show-default-option="false" data-region-id="region">
                                        @if($errors->has('country'))
                                            <div class="error">*@lang('main.registration.wrong')</div>
                                        @endif
                                    </select>
                                    <select id="region" class="display-none"></select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="register__form--raw">
                                    <label class="title bold" for="city">@lang('main.registration.city')</label>
                                    <input type="text" value="{{$data->city}}" name="city" />
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="register__form--raw">
                                    <label class="title bold" for="contact">@lang('main.registration.extra')*</label>
                                    <input required="required" type="text" name="extra_phone" value="{{$data->extra_phone}}" id="contact">
                                </div>
                                <div class="info">
                                    @lang('main.registration.info')
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="register__form--raw">
                                    <label class="title" for="size">@lang('main.registration.size')</label>
                                    <select name="size" id="size">
                                        <option @if($data->size == 'XXL') selected @endif  value="XXL">XXL</option>
                                        <option @if($data->size == 'XL') selected @endif value="XL">XL</option>
                                        <option @if($data->size == 'L') selected @endif value="L">L</option>
                                        <option @if($data->size == 'M') selected @endif value="M">M</option>
                                        <option @if($data->size == 'S') selected @endif value="S">S</option>
                                        <option @if($data->size == 'XS') selected @endif value="XS">XS</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12">
                            <div class="register__change">
                                @lang('main.profiles.change')
                            </div>
                                <div class="col-md-6">
                                    <div class="register__form--raw">
                                        <label class="title bold" for="password">@lang('main.profiles.old')*</label>
                                        <input type="password" id="password" name="password">
                                        @if($errors->has('password'))
                                            <div class="error">{{$errors->first('password')}}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="register__form--raw">
                                        <label class="title bold" for="password1">@lang('main.profiles.new')*</label>
                                        <input type="password" id="password1" name="new_password">
                                        @if($errors->has('new_password'))
                                            <div class="error">{{$errors->first('new_password')}}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="register__form--raw">
                                        <label class="title bold" for="password2">@lang('main.profiles.confirm')*</label>
                                    <input type="password" id="password2" name="new_password_confirmation">
                                        @if($errors->has('new_password_confirmation'))
                                            <div class="error">{{$errors->first('new_password_confirmation')}}</div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <button class="main__register">
                                    @lang('main.save')
                                </button>
                                <a href="{{action('AuthController@logout')}}" class="main__logout">
                                    @lang('main.profiles.logout')
                                </a>
                            </div>
                        </form>
                    <div class="clearfix"></div>
                    </div>
                </div>
                <div class="col-md-3 hidden-sm">
                        <br><br><br><br><br>
                        <div class="content__btn">
                            @lang('main.profiles.photo')
                        </div>
                        <br><br>
                        <div class="content__btn">
                            @lang('main.profiles.certificate')
                        </div>
                    <div class="profile-right">
                        <div class="profile-right__title">
                            @lang('main.profiles.info')
                        </div>
                        <div class="profile-right__info">
                            <div class="left">@lang('main.registration.distances'):</div>
                            <div class="right">{{$data->distance ? $data->distance->long : null}}</div>
                        </div>
                        <div class="profile-right__info">
                            <div class="left">@lang('main.registration.start'):</div>
                            <div class="right">{{$data->distance ? \Carbon\Carbon::parse($data->distance->date)->locale(app()->getLocale())->isoFormat('D MMMM') : null}}</div>
                        </div>
                        <div class="profile-right__info">
                            <div class="left">@lang('main.registration.place'):</div>
                            <div class="right">{{app()->getLocale() == 'ru' ? $data->distance->place : (app()->getLocale() == 'en' ? $data->distance->place_en : $data->distance->place_uz) }}</div>
                        </div>
                        <div class="profile-right__info">
                            <div class="left">@lang('main.registration.times'):</div>
                            <div class="right">{{$data->distance ? $data->distance->time : null}}</div>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
</div>
<br><br><br><br>
@endsection
@section('script')
    <script src="{{asset('/js/jquery.crs.min.js')}}"></script>
    <script src="{{asset('/js/intlTelInput-jquery.min.js')}}"></script>
    <script>
    $(document).ready(function() {
      $('#country option[value="{{ $data->country }}"]').prop("selected", true).parent().trigger('change');
    })
    $('.latin').bind('keyup',function(){
        $(this).val($(this).val().replace(/[^a-z ]/i, ""))
    });
      $('#tel').intlTelInput({
            nationalMode: false,
            utilsScript: "{{asset('js/utils.js')}}",
            preferredCountries: [ "uz" ],
      });
      $('#contact').intlTelInput({
            nationalMode: false,
            utilsScript: "{{asset('js/utils.js')}}",
            preferredCountries: [ "uz" ],
      });
    </script>
@endsection
