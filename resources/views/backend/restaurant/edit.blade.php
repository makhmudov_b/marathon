@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Изменить Ресторан</div>
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <form action="{{ action('RestaurantController@update', $rest->id) }}" method="POST" enctype="multipart/form-data" >
                            @csrf
                            <div class="form-group">
                                <label for="restaurant-name">Название</label>
                                <input required="required" value="{{ $rest->name }}" name="name" type="text" class="form-control" id="restaurant-name">
                            </div>
                            <div class="form-group">
                                <label for="restaurant-desc-uz">Описание (Uz)</label>
                                <input required="required" value="{{ $rest->desc_uz }}" name="desc_uz" type="text" class="form-control" id="restaurant-desc-uz">
                            </div>
                            <div class="form-group">
                                <label for="restaurant-desc-ru">Описание (Ru)</label>
                                <input required="required" value="{{ $rest->desc_ru }}" name="desc_ru" type="text" class="form-control" id="restaurant-desc-ru">
                            </div>
                            @if(!Auth::user()->hasRole('manager'))
                            <div class="form-group">
                                Выберите категорию
                                <select multiple name="category_id[]" class="chosen-select form-control">
                                    @foreach( $data as $datas )
                                    <option value="{{ $datas->id }}">{{ $datas->name_ru }}</option>
                                    @endforeach
                                </select>
                                </div>
                            <div class="form-group">
                                Выберите район
                                <select name="district_id" class="form-control">
                                    @foreach( $dist as $datas )
                                    <option value="{{ $datas->id }}">{{ $datas->name_ru }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="restaurant-adress">Адресс</label>
                                <input required="required" value="{{ $rest->address }}" name="address" type="text" class="form-control" id="restaurant-adress">
                            </div>
                            <div class="form-group">
                                <label>Longitude</label>
                                <input required="required" value="{{ $rest->long }}" name="long" type="text" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Lattitude</label>
                                <input required="required" value="{{ $rest->lat }}" name="lat" type="text" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="food-cook">Телефон</label>
                                <input required="required" value="{{ $rest->phone }}" name="phone" type="number" class="form-control" id="restaurant-phone">
                            </div>
                            <div class="form-group">
                            <label class="custom-switch">
                                <span class="mr-3">Бесплатная Доставка</span>
                                <input type="checkbox" name="custom-switch-checkbox" class="custom-switch-input" name="shipping_free" value="true">
                                <span class="custom-switch-indicator"></span>
                            </label>
                            </div>
                            <div class="form-group">
                            <label class="custom-switch">
                                <span class="mr-3">На Главной</span>
                                <input type="checkbox" name="custom-switch-checkbox" class="custom-switch-input" name="in_main" value="true">
                                <span class="custom-switch-indicator"></span>
                            </label>
                            </div>
                            <div class="form-group">
                                <div class="form-label">Способы Оплаты</div>
                                <div class="custom-switches-stacked">
                                <label class="custom-switch">
                                    <input type="radio" name="payments" value="[0]" class="custom-switch-input" checked>
                                    <span class="custom-switch-indicator"></span>
                                    <span class="custom-switch-description">Наличные</span>
                                </label>
                                <label class="custom-switch">
                                    <input type="radio" name="payments" value="[0 , 1]" class="custom-switch-input">
                                    <span class="custom-switch-indicator"></span>
                                    <span class="custom-switch-description">Наличные/Payme</span>
                                </label>
                                </div>
                            </div>
                            <div class="form-group">
                                    <img src="{{ asset('uploads/restaurant/'.$rest->id.'.jpg') }}" width="140">
                                    Для изменения лого загрузите новое фото
                                    <input type="file" class="form-control" name="image">
                            </div>
                            <div class="form-group">
                                    <img src="{{ asset('uploads/restcover/'.$rest->id.'.jpg') }}" width="140">
                                    Для изменения Ковэра загрузите новое фото
                                    <input type="file" class="form-control" name="cover">
                            </div>
                            @endif
                            <button class="btn btn-success">Сохранить</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
<script src="{{ asset('backend/js/vendors/jquery-3.2.1.min.js') }}"></script>
<script src="{{ asset('backend/js/vendors/chosen.js') }}"></script>
<script>
      $(function() {
        $('.chosen-select').chosen();
        $('.chosen-select-deselect').chosen({ allow_single_deselect: true });
      });
</script>
@endsection
@section('style')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.4.2/chosen.css">
@endsection
