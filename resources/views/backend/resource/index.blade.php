@extends('layouts.backend')

@section('content')
<div class="container">
<div class="card my-3 my-md-5">
    <div class="card-header justify-content-between">
    <h3 class="card-title">Ресурсы</h3>
        <a class="btn btn-outline-success" href="{{ action('PanelController@create') }}">Добавить</a>
    </div>
    <div class="table-responsive">
    <table class="table card-table table-vcenter text-nowrap">
        <thead>
        <tr>
            <th>No</th>
            <th>Категория</th>
            <th>Название (RU)</th>
            <th>Описание (RU)</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        @foreach($data as $datas)
        <tr>
            <td>{{ $datas->id }}</td>
            <td>{{$datas->category->name }}</td>
            <td>{{$datas->title }}</td>
            <td>{{$datas->description   }}</td>
            <td>
                <form action="{{ action('PanelController@destroy' , $datas->id) }}" onclick="return confirm('Вы уверены?')" method="POST">
                @method('DELETE')
                @csrf
                <button class="btn icon border-0">
                    <i class="fe fe-trash"></i>
                </button>
                </form>
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>
    </div>
</div>
</div>
@endsection
