@extends('layouts.app')

@section('content')
<div class="container">
<div class="card my-3 my-md-5">
    <div class="card-header justify-content-between">
    <h3 class="card-title">Заказы</h3>
        <a href="{{ action('OrderController@import') }}">Excel</a>
    </div>
    <div class="table-responsive">
    <table class="table card-table table-vcenter text-nowrap">
        <thead>
        <tr>
            <th class="w-1">No.</th>
            <th>Ресторан</th>
            <th>Пользователь</th>
            <th>Адресс</th>
            <th>Дата</th>
            <th>Статус</th>
            <th>Общая сумма</th>
            <th></th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        @foreach($data as $datas)
        <tr>
            <td>#{{ $datas->id }}</td>
            @if($datas->restaurant_id !== 0)
            <td>{{ $datas->restaurant->name }}</td>
            @else
            <td>Не выбран</td>
            @endif
            @if($datas->user->phone !== null)
                <td>{{ $datas->user->phone }}</td>
            @else
                <td>Нет Номера</td>
            @endif
            <td>{{ $datas->adress }}</td>
            <td>{{ \Carbon\Carbon::parse($datas->updated_at)->toDateTimeString() }}</td>
            @if($datas->status == 0)
                <td>В корзине</td>
            @endif
            @if($datas->status == 1)
                <td>Оформлен</td>
            @endif
            @if($datas->status == 2)
                <td>Принят</td>
            @endif
            @if($datas->status == 3)
                <td>Оплачен</td>
            @endif
            @if($datas->status == 4)
                <td>Доставлен</td>
            @endif
            <td>{{ $datas->total_price }}</td>
            @if(Auth::user()->hasRole('admin'))
            <td><a href="{{ action('ReviewController@index' , $datas->id) }}" class="btn btn-secondary btn-sm">Отзыв</a></td>
            @else
            <td></td>
            @endif
            <td>
            @if($datas->restaurant_id !== 0)
            <a href="{{ action('OrderController@showInner', $datas->id)  }}" class="btn btn-secondary btn-sm">
                <i class="fe fe-arrow-right"></i>
            </a>
            @endif
            </td>
        </tr>
        @endforeach        
        </tbody>
    </table>
    </div>
</div>
</div>        
</div>
@endsection
