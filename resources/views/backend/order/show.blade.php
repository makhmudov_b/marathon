@extends('layouts.app')

@section('content')
<div class="my-3 my-md-5">
<div class="container">
<div class="row row-cards">
  <div class="col-lg-3">
    <div class="row">
      <div class="col-md-6 col-lg-12">
        <div class="card">
          <div class="card-body">
            <div class="mb-4 text-center">
              <img src="{{ $data->restaurant->image  }}" class="img-fluid">
            </div>
            <h4 class="card-title"><a href="javascript:void(0)">{{ $data->restaurant->name  }}</a></h4>
            <div class="card-title">
              Статус:
            @if($data->status == 0)
                В корзине
            @endif
            @if($data->status == 1)
                Оформлен
            @endif
            @if($data->status == 2)
                Принят
            @endif
            @if($data->status == 3)
                В пути
            @endif
            @if($data->status == 3)
                Доставлен
            @endif
            </div>
            <div class="card-subtitle">
              Адресс: {{ $data->restaurant->address  }}
            </div>
            <div class="card-subtitle">
              Метод Оплаты:
              @if($data->payment_method == 1)
                Payme
              @else
                Наличные
              @endif
            </div>
            @if($data->status == 1)
            <div class="text-center">
              <form method="POST" class="d-inline" action="{{ action('OrderController@successOrder',$data->id) }}">
                @method('PUT')
                @csrf
                <button type="submit" class="btn btn-outline-success">Принять</button>
              </form>
              <form method="POST" class="d-inline" action="{{ action('OrderController@deleteOrder',$data->id) }}">
                @method('DELETE')
                @csrf
                <button type="submit" class="btn btn-outline-danger">Отменить</button>
              </form>
            </div>
            @endif
          </div>
        </div>
      </div>
      <div class="col-md-6 col-lg-12">
        <div class="card">
          <div class="card-body">
            <h4 class="card-title"><a href="javascript:void(0)">Пользователь : {{ $data->user->name  }}</a></h4>
            <div class="card-subtitle">
              Адресс: {{ $data->user->adress  }}
            </div>
            <div class="card-subtitle">
              Телефон: {{ $data->user->phone  }}
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-lg-9">
    <div class="card">
      <table class="table card-table table-vcenter">
        <tbody>
        @foreach($data->order_food as $datas)
        <tr>
          @if($datas->food !== null)
          <td><img src="{{ $datas->food->image  }}" class="h-8"></td>
          <td>
            {{  $datas->food->name_ru }}
          </td>
            @else
            <td>Удалено</td>
            <td></td>
          @endif
          <td class="text-right text-muted d-none d-md-table-cell text-nowrap">{{  $datas->amount }} шт</td>
          <td class="text-right">
            <strong>{{  $data->total_price - $data->shipping_price }} сум</strong>
          </td>
        </tr>
        @endforeach
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td class="text-right">Стоимость доставки : <strong>{{ $data->shipping_price  }} сум</strong></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td class="text-right">Стоимость заказа : <strong>{{ $data->total_price  }} сум</strong></td>
        </tr>
      </tbody>
      </table>
    </div>
  </div>
</div>
</div>
</div>
@endsection
