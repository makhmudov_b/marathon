<p>Please scroll down for the English version.</p>
<p>O'zbek tilidagi versiya uchun, iltimos, pastga tushuring.</p>
<br><br>
<p>Здравствуйте!</p>
<p>Спешим сообщить, что вы официально зарегистрировались на благотворительном полумарафоне в Самарканде. Теперь у вас есть свой личный кабинет на нашем сайте <a href="http://www.samarkandhalfmarathon.uz/"><span><u><span>www.</span></u></span><span><u>samarkandhalfmarathon.uz</u></span></a>. Советуем проверить данные анкеты &ndash; эти данные важны, потому что мы по ним будем выдавать стартовые номера всем участникам забега.</p>
<p>Напоминаем, что сам забег пройдет 3 ноября. Мы готовим специальную культурную программу, о которой будем сообщать на нашем <span><u><a href="http://www.samarkandhalfmarathon.uz/">сайте</a></u></span> и социальных сетях. Подписывайтесь на нас в <span><u><a href="https://www.facebook.com/samarkandhalfmarathon/" target="_blank"><span>Facebook</span></a></u></span><span>, </span><span><u><a href="https://www.instagram.com/samarkandhalfmarathon/"><span>Instagram</span></a></u></span><span> и </span><span><u><a href="https://t.me/joinchat/AAAAAFiSBZ0S2cRwdFnTQw" target="_blank"><span>Telegram</span></a></u></span>.</p>
<p>Выдача стартовых номеров будет осуществляться в Самарканде:</p>
<p>1 ноября с 10:00 до 19:00</p>
<p>2 ноября с 10:00 до 19:00</p>
<p>В день забега стартовые номера не выдаются!</p>
<p>Время сбора участников 3 ноября в 8:00</p>
<p>Старт забега в 9.00</p>
<p>Если возникнут вопросы, не стесняйтесь, свяжитесь с нами. Вот контакты:</p>
<p>halfmarathon@acdf.uz</p>
<p>+998 99 3632236</p>
<p>P.S. Будем благодарны, если вы разместите информацию о полумарафоне в своих социальных сетях. Зовите друзей в Самарканд, вместе будет веселее!</p>
<p>Остаемся на связи,</p>
<p>Команда Samarkand Half Marathon</p>
<br><br>
<p><span>Assalomu alaykum!</span></p>
<p><span>Mamnuniyat bilan ma'lum qilamiz, Siz Samarqandda bo&rsquo;lib o&rsquo;tadigan xayriya yarim marafoniga rasmiy ro'yxatdan o'tdingiz. Endi Siz veb-saytimizda shaxsiy kabinetingizga egasiz </span><span><u><a href="http://www.samarkandhalfmarathon.uz/"><span>www.samarkandhalfmarathon.uz</span></a></u></span><span>. Sizga anketa ma'lumotlarini tekshirishingizni maslahat beramiz. Bu ma'lumotlar juda muhim, chunki biz yugurish qatnashchilariga start raqamlarini anketa asosida beramiz.</span></p>
<p><span>Eslatib o'tamiz, yugurish musobaqasi 3-noyabr kuni bo'lib o'tadi. Bundan tashqari, biz madaniyat dasturini ham tayyorlamoqdamiz va u haqida </span><span><u><a href="http://www.samarkandhalfmarathon.uz/"><span>veb-saytimiz</span></a></u></span><span> hamda ijtimoiy tarmoqlarda xabar beramiz. Bizni </span><span><u><a href="https://www.facebook.com/samarkandhalfmarathon/" target="_blank"><span>Facebook</span></a></u></span><span>, </span><span><u><a href="https://www.instagram.com/samarkandhalfmarathon/"><span>Instagram</span></a></u></span><span> va </span><span><u><a href="https://t.me/joinchat/AAAAAFiSBZ0S2cRwdFnTQw" target="_blank"><span>Telegram</span></a></u></span> kuzatib boring.</p>
<p><span>Start raqamlarini tarqatish Samarqand shahrida amalga oshiriladi:</span></p>
<p><span>1-noyabr soat 10:00 dan 19:00 gacha</span></p>
<p><span>2-noyabr soat 10:00 dan 19:00 gacha</span></p>
<p><span>Musobaqa kuni start raqamlari berilmaydi!</span></p>
<p><span>Ishtirokchilar yig'ilish vaqti: 3-noyabr, soat 8:00</span></p>
<p><span>Musobaqa 9.00 da boshlanadi</span></p>
<p><span>Agar biror savol bo'lsa, tortinmang, biz bilan bog'laning. Aloqa uchun ma&rsquo;lumotlar:</span></p>
<p><span>halfmarathon@acdf.uz</span></p>
<p><span>+998 99 3632236</span></p>
<p><span>P.S. Yarim marafon haqida ma'lumotni ijtimoiy tarmoqlardagi sahifalaringizga joylashtirsangiz juda minnatdor bo&rsquo;lamiz. Do&rsquo;stlaringizni Samarqandga taklif qiling, birgalikda yanada qiziqarli bo'ladi!</span></p>
<p><span>Hurmat bilan,</span></p>
<p>Samarkand Half Marathon jamoasi</p>
<br><br>
<p><span>Hello!</span></p>
<p><span>We are glad to inform that you have officially registered for the Charity Half Marathon in Samarkand. Now you have your personal account on our website </span><span><u><a href="http://www.samarkandhalfmarathon.uz/"><span>www.samarkandhalfmarathon.uz</span></a></u></span><span> . We advise you to check the data of the questionnaire - this data is important, because according to it we will give out start numbers to all participants of the race.</span></p>
<p><span>We remind you that the race itself will be held on November 3. We are preparing a special cultural program, about which we will report on our </span><span><u><a href="http://www.samarkandhalfmarathon.uz/"><span>website</span></a></u></span><span> and social networks. Follow us on </span><span><u><a href="https://www.facebook.com/samarkandhalfmarathon/" target="_blank"><span>Facebook</span></a></u></span><span>, </span><span><u><a href="https://www.instagram.com/samarkandhalfmarathon/"><span>Instagram</span></a></u></span> and <span><u><a href="https://t.me/joinchat/AAAAAFiSBZ0S2cRwdFnTQw" target="_blank"><span>Telegram</span></a></u></span>.</p>
<p><span>Issue of start numbers will be held in Samarkand:</span></p>
<p><span>November 1 from 10:00 to 19:00</span></p>
<p><span>November 2 from 10:00 to 19:00</span></p>
<p><span>On the day of the race, start numbers are not issued!</span></p>
<p><span>The time for the gathering of participants is November 3 at 8am</span></p>
<p><span>The race starts at 9am</span></p>
<p><span>If you have any questions, feel free to contact us. Here are the contacts:</span></p>
<p><span>halfmarathon@acdf.uz</span></p>
<p><span>+998 99 3632236</span></p>
<p><span>P.S. We will be grateful if you post information about the half marathon on your social networks. Call your friends in Samarkand, together it will be more fun!</span></p>
<p><span>Let&rsquo;s keep in touch,</span></p>
<p><span>Samarkand Half Marathon Team</span></p>
