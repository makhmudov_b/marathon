<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="Content-Language" content="{{ app()->getLocale() }}" />
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"/>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">
    <meta property="og:title" content="Samarkand Half Marathon">
    <meta property="og:description" content="Samarkand Half Marathon">
    <meta property="og:image" content="{{asset('img/meta.jpg')}}">
    <meta property="og:url" content="https://samarkandhalfmarathon.uz">
    <meta name="title" content="Самаркандский Полумарафон"/>
    <meta name="description" content="Первый международный благотворительный забег в Узбекистане. Uzbekistan's First International Charity Run"/>
    <title>Samarkand Half Marathon</title>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,700,700i,900,900i&display=swap&subset=cyrillic" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="{{ asset('css/styles.css') }}">
</head>
    <body>
        <div class="wrapper">
            @include('partials.nav')
            @include('partials.menu')
                @yield('content')
                <nav class="nav little-hide">
                    <div class="container">
                        <div class="nav__links">
                            <a href="{{ action('PageController@about')  }}">@lang('main.nav.1')</a>
                            <a href="{{ action('PageController@information')  }}">@lang('main.nav.2')</a>
                            <a href="{{ action('PageController@program_first')  }}">@lang('main.nav.3')</a>
                            <a href="{{ action('PageController@runners')  }}">@lang('main.nav.4')</a>
{{--                            <a href="{{ action('PageController@summary')  }}">@lang('main.nav.5')</a>--}}
                            <a href="{{ action('PageController@contacts') }}">@lang('main.nav.6')</a>
                        </div>
                    </div>
                </nav>
                <div class="site-bottom">
                <div class="container">
                    <div class="col-md-9">
                        <h3 class="site-bottom__title">
                            @lang('main.part')
                        </h3>
                    </div>
                    <div class="col-md-3 text-right">
                        <a href="{{action('PageController@register')}}" class="site-bottom__btn main__register">
                            @lang('main.register')
                        </a>
                    </div>
                </div>
                    @include('partials.footer')
            </div>
        </div>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script>
            $('#profile').click(function () {
                $('.auth-modal').toggleClass('is-show')
                $('.wrapper').toggleClass('fixed')
                $('.auth').toggleClass('is-show')
            })
            $('.auth-modal').click(function () {
                $('.auth-modal').removeClass('is-show')
                $('.wrapper').removeClass('fixed')
                $('.auth').removeClass('is-show')
            })
            // var offset = $("#profile").offset();
            // var el = $('#profile').width();
            // $(window).on('resize',function () {
            //     $('.auth').css({
            //         'background':'black',
            //         'left': offset.left - el
            //     })
            // })


            $('.hamburger').click(function () {
                $('.toggle').toggleClass('display-none')
                $('.modal').toggleClass('is-show')
                $('.wrapper').toggleClass('fixed')
                $('.menu').toggleClass('is-open')
            })
            $('.modal').click(function () {
                $('.toggle').removeClass('display-none')
                $('.modal').removeClass('is-show')
                $('.wrapper').removeClass('fixed')
                $('.menu').removeClass('is-open')
            })
        </script>
        <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <script type="text/javascript">
          @if(session()->has('success'))
           swal({
              title: "@lang('main.success')!",
              text: "{{ session()->get('success') }}",
              icon: "success",
              button: "OK",
          });
          @endif
          @if(session()->has('error'))
           swal({
              title: "@lang('main.error')!",
              text: "{{ session()->get('error') }}",
              icon: "error",
              button: "OK",
          });
          @endif
        @if (session('status'))
           swal({
              title: "@lang('main.success')!",
              text: "{{ session('status') }}",
              icon: "info",
              button: "OK",
          });
        @endif
        </script>
    @yield('script')
    </body>
</html>
