@extends('layouts.frontend')

@section('content')
    <div class="register mt-50 mb-60">
        <div class="container">
            <div class="col-md-6 col-md-push-3 py-layout pb-10 pt-10" style="box-shadow: 0px 4px 50px rgba(0, 0, 0, 0.1);">
                <div class="auth__title">@lang('main.reset')</div>
                <div class="card-body">
                    <form method="POST" class="register__form" action="{{ route('password.email') }}">
                        @csrf
                        <div class="register__form--raw text-center">
                            <label for="email" class="title text-left">{{ __('E-Mail') }}</label>
                            <input id="email" type="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>
                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                            <br><br>
                            <button type="submit" class="main__register" style="margin-top: 0;">
                                @lang('main.reset')
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
</div>
@endsection
