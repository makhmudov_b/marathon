<!doctype html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta http-equiv="Content-Language" content="en" />
    <meta name="msapplication-TileColor" content="#2d89ef">
    <meta name="theme-color" content="#4188c9">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"/>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">
    <title>OnlineTaom::Back</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,300i,400,400i,500,500i,600,600i,700,700i&amp;subset=latin-ext">
    <script src="{{ asset('backend/js/require.min.js')}}"></script>
    <script>
      requirejs.config({
          baseUrl: '/'
      });
    </script>
    <link href="{{ asset('backend/css/dashboard.css')}}" rel="stylesheet" />
    <script src="{{ asset('backend/js/dashboard.js')}}"></script>
    <link href="{{ asset('backend/plugins/charts-c3/plugin.css')}}" rel="stylesheet" />
    <script src="{{ asset('backend/plugins/charts-c3/plugin.js')}}"></script>
    <link href="{{ asset('backend/plugins/maps-google/plugin.css')}}" rel="stylesheet" />
    <script src="{{ asset('backend/plugins/maps-google/plugin.js')}}"></script>
    <script src="{{ asset('backend/plugins/input-mask/plugin.js')}}"></script>
  </head>
  <body class="">
    <div class="page">
      <div class="page-single">
        <div class="container">
          <div class="row">
            <div class="col col-login mx-auto">
              <div class="text-center mb-6">
              <img src="{{ asset('/img/logo.svg') }}" class="h-6" alt="">
              </div>
              <form class="card" action="{{ route('login') }}" method="POST">
                @csrf                
                <div class="card-body p-6">
                  <div class="card-title" style="text-align: center;">Панель управления</div>
                  <div class="form-group">
                    <label class="form-label">E-mail</label>
                    <input type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" id="exampleInputEmail1" value="{{ old('email') }}" aria-describedby="emailHelp" placeholder="Введите e-mail">
                    @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                  </div>
                  <div class="form-group">
                    <label class="form-label">
                      Пароль
                    </label>
                    <input type="password" name="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" id="exampleInputPassword1" placeholder="Пароль" required="required">
                    @if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif                    
                  </div>
                  <div class="form-footer">
                    <button type="submit" class="btn btn-primary btn-block">Войти</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </body>  
</html>
