<?php

return [
    'nav' => [1 => 'POYGA HAQIDA', 2 => 'Qatnashchilar uchun', 3 => 'DASTUR', 4 => 'KIM BOShQARADI' , 5 => 'Natija' , 6 => 'ALOQALAR'],
    'title' => 'Забег <br/> По Самарканду',
    'bottom' => 'Давай с нами, <br/>   будет круто!',
    'register' => 'RO’YXATDAN O’TISH',
    'register_now' => 'HOZIROQ  RO’YXATDAN O’TING!',
    'profile' => ['cabinet'=>'SHAXSIY KABINET'],
    'part'=>'YARIM MARATHONDA HOZIROQ ISHTIROK ETING!',
    'save'=>'Saqlash',
    'content' => [
        'title' => 'POYGA BOSHLANISHIGA',
        'runners' => 'BIZ BILAN YUGURMOQDA',
        'contacts' => 'ALOQALAR',
        'organizers' => 'TASHKILOTCHILAR',
        'general' => 'BOSH HAMKORLAR',
        'fitness' => 'HAMKORLAR',
        'sponsor' => 'Homiylar',
        'interesting' => 'Интересное',
        'all' => 'BARCHA QATNASHUVCHILAR',
        'material' => 'Все материалы',
        'partners' => 'Партнеры',
        'social' => 'YANGILIKLARIMIZNI KUZATIB BORING',
        'also' => 'Читайте также',
    ],
    'footer' => [
        'fond' =>
               'O’zbekiston Respublikasi Madaniyat vazirligi huzuridagi Madaniyat va san’atni rivojlantirish Jamg’armasi<br>
                Manzil: 100029, Taras Shevchenko ko’ch., 1-uy.<br>
                Reg № 552081 <br>
                STIR: 305122818 ',
        'mail' => 'ELEKTRON POCHTA',
        'phone' => 'TELEFON RAQAMLARIMIZ <br> 9:00 – 18:00 dushanbadan jumagacha <br>',
        'social' => 'IJTIMOIY TARMOQLAR ORQALI  <br> YANGILIKLARIMIZNI KUZATIB BORING',
    ],
    'registration' => [
        '1' => 'MENING ARIZAM',
        '2' => 'TO’LOV',
        '3' => 'TASDIQLOVCHI MA’LUMOT',
        'email' => 'Ushbu elektron pochta ishlatilgan',
        'phone' => 'МОBIL ТЕLEFON',
        'phoneror' => 'Ushbu raqam ishlatilgan',
        'name' => 'ISM',
        'lastname' => 'FAMILIYA',
        'birthday' => 'TUG’ILGAN SANA VA YIL',
        'gender' => 'JINSI',
        'male' => 'Ayol',
        'female' => 'Erkak',
        'wrong' => 'Noto’g’ri qiymat kiritilgan',
        'country' => 'MAMLAKAT',
        'city' => 'SHAXAR',
        'extra' => 'FAVQULOD HOLDAGI TELEFON ALOQA',
        'info' => 'To’ldirishda diqqatli bo’ling! Agar siz bilan biror kor hol yuz bersa, ushbu telefon raqami ostida ko’rsatilgan shaxs bilan bog’laniladi.',
        'size' => 'FUTBOLKANING O’LCHAMI',
        'promo' => 'PROMO-KOD',
        'password' => 'PAROL',
        'warn' => 'To’ldirishda ehtiyot va diqqatli bo’ling! Siz kiritgan parol profilingizga kirish uchun ishlatiladi.',
        'distance' => 'MASOFANI TANLASH',
        'sum' => 'som',
        'time' => 'REJALASHTIRILGAN VAQT',
        'a1' => 'Klaster A - 50 daqiqadan tezroq',
        'b1' => 'Klaster B - 50 dan 60 daqiqagacha',
        'c1' => 'Klaster C - 60 daqiqadan ko’proq vaqt, va birinchi marta yugurishda qatnashganim tufayli, masofani bosib o’tadigan vaqtimni bilmayman',
        'a2' => 'Klaster A - 1 soatu 40 daqiqadan tezroq',
        'b2' => 'Klaster B - 1 soat 40 daqiqadan 2 soatgacha',
        'c2' => 'Klaster C - 2 soatdan ko’proq vaqt va birinchi marta yugurishda qatnashganim tufayli, masofani bosib o’tadigan vaqtimni bilmayman',
        'invalid' => 'Nogironlik',
        'check' => 'Rasmiy ravishda tasdiqlangan nogironligingiz bo’lsa va siz poygada yugurishga qaror qilgan bo’lsangiz, nogironligingizni ro’yxatdan o’tayotganingizda belgilab o’ting.',
        'agreement' =>
            '
                Ishtirok etishim natijasida salomatligim uchun turli zararli oqibatlar kelib chiqishi mumkinligini anglab yetganman. Jismoniy ahvolim yaxshi ekani va mazkur musobaqada qatnashish uchun yetarli darajada tayyorgarlik ko’rganimga guvohlik beraman va buni tasdiqlayman, salomatligim ahvoli litsenziyaga ega bo’lgan shifokor tomonidan tasdiqlangan. 
                    <br><br>
                 Musobaqalar chog’ida yoki undan keyin bahtsiz hodisalar ro’y berganda, jarohat olgan yoki jismoniy shikast yetgan taqdirda men 2019 yilning 3-noyabr kuni bo’lib o’tadigan “Samarkand Half Marathon”ga qarshi va ularning agentlari, hodimlari, mansabdor shaxslari, direktorlari, vorislari, homiylari, etkazib beruvchilari va vakillariga yugurishda va har qanday oldingi va keying yugurish tadbirlarida ishtirokim natijasida kelib chiqishi mumkin bo’lgan biror-bir moddiy yoki boshqa da’volardan o’z xohishimga binoan oldindan voz kechaman. Mazkur da’volardan voz kechish menga, mening merosxo’rlarimga, ijrochilarimga, shaxsiy vakillarimga, vorislarimga, huquqiy vorislarimga taalluqli hisoblanadi.
                    <br><br>
                 Bu bilan har kimga va yuqorida qayd etilganlarga mening yugurish jarayonida olingan rasmimdan istalgan fotosuratlarda, videoroliklarda yoki shu hodisaning istalgan yozuvlarida har qanday qonuniy maqsadlarda, xususan, tijoriy reklamada foydalanishga to’liq ruxsat beraman.            
            ',
        'agree1' => 'Men xizmat shartlari bilan tanishib chiqdim va ',
        'agree2' => 'ularga norozilik bildirmayman',
        'save' => 'RO’YXATDAN O’TISH',
        'pay' => 'MENING BOSHLANG’ICH TO’LOVIM',
        'method' => 'To’lovning har qanday usullari:',
        'about' => 'TADBIR TO’G’RISIDA',
        'distances' => 'MASOFA',
        'start' => 'START SANASI',
        'place' => 'JOYI',
        'times' => 'VAQTI',
    ],
    'contacts' => [
        '1' => 'Volontyor sifatida ishtirok etish uchun:',
        '2' => '("volontyor" mavzusidagi xat bilan)',
        '3' => 'Ishtirok etish uchun ro’yxatga olish va to’lov bilan bog’liq texnik masalalar bo’yicha',
        '4' => '("ro’yxatga olish" mavzusidagi xat bilan)',
        '5' => 'OAVni akkreditatsiyadan o’tkazish masalalari bo’yicha',
        '6' => 'Akkreditatsiyadan o’tish uchun arizani to’ldirishingiz kerak.',
        '7' => 'Hamkorlik masalalari bo’yicha',
        '8' => '("hamkorlik" mavzusidagi xat bilan)',
        '9' => 'Ta’lim komponenti masalalari bo’yicha',
        '10' => '("ro’yxatga olish" mavzusidagi xat bilan)',
    ],
    'about' => [
        '1' => 'Poyga tashkilotchisi',
        '2' => 'O’zbekiston Respublikasi Madaniyat vazirligi huzuridagi Madaniyat va san’atni rivojlantirish jamg’armasi ikki yildan buyon san’at, ta’lim, madaniyat muassasalariga moddiy-texnik ko’maklashish va homiylikni rivojlantirish sohasidagi loyihalarni amalga oshirmoqda.',
        '3' => 'Jamg’arma faoliyati haqida batafsil ma’lumotni bizning saytimizda topishingiz mumkin',
        '4' => 'O’zbekistonda birinchi xalqaro xayriya poygasi',
        '5' => '“Samarkand Half Marathon” 2019-yil 1-noyabrdan 3-noyabrgacha Samarqand shahrida o’tkaziladi. Loyihaning asosiy maqsadi madaniyat va san’at ob’ektlarida inklyuzivlik muammosiga jamoatchilik e’tiborini qaratish, shuningdek, sog’lom turmush tarzini ommalashtirish va O’zbekistonda sport turizmini rivojlantirishdan iborat.',
        '6' => 'Yarim marafon doirasida inklyuzivlik mavzusiga bag’ishlangan bir qator ta’lim tadbirlari o’tkaziladi. Ular orasida bolalar va kattalar uchun konferentsiyalar, ma’ruzalar va mahorat darslari tashkil etiladi. Dastur 2019-yil sentyabr oyida batafsil e’lon qilinadi.',
        '7' => 'Yarim marafondan tushgan mablag’lar Toshkent teatrlaridan birida ko’r va ko’zi ojiz insonlar uchun qulay muhit yaratish bo’yicha eksperimental loyihani amalga oshirishga yo’naltiriladi. Ushbu dastur tifloizohlash (audiodeskripsiya) yordamida pyesani teatr sahnasida namoyish etilishini o’z ichiga oladi. Buning uchun zarur uskunalarning xaridi, shuningdek, mutaxassislarni o’qitilish tashkil etiladi.',
        '8' => 'Yarim marafonga tayyorgarlik jarayonida madaniyat va san’atni rivojlantirish jamg’armasi inklyuzivlik muammosiga bag’ishlangan tadqiqotni o’tkazadi – O’zbekiston teatrlari, muzeylari va kutubxonalarida treninglar va monitoring o’tkaziladi. Tadqiqot natijalari yarim marafonning ta’lim dasturi doirasida hisobot shaklida taqdim etiladi.',
    ],
    'profiles' => [
        'main' => 'UMUMIY MA’LUMOT',
        'personal' => 'SHAXSIY MA’LUMOTLAR',
        'change' => 'PAROLNI O’ZGARTIRISH',
        'old' => 'OLDINGI PAROLINGIZGIZ',
        'new' => 'YANGI PAROL',
        'confirm' => 'YANA BIR BOR YANGI PAROL',
        'logout' => 'CHIQING',
        'photo' => 'MENING RASMLARIM',
        'certificate' => 'MENING SERTIFIKATIM',
        'info' => 'MENING YUGURISH MUSOBAQALARIM',
    ],
    'forget' => 'Parolni eslash',
    'login' => 'Kirish',
    'reset' => 'Parolni yangilash',
    'success' => 'Muvaffaqiyatli',
    'error' => 'Xato',
    'suclog' => 'Shaxsiy kabinetingizga muvaffaqiyatli kirdingiz',
    'errlog' => 'Foydalanuvchi nomi yoki parol noto’g’ri',
    'sucpromo' => 'Muvaffaqiyatli royxtdan otdingiz promokod:',
    'change' => 'Ma’lumotlaringiz muvaffaqiyatli o’zgartirildi',
    'logout' => 'Muvaffaqiyat bilan shaxsiy kabinetingizni tark etdingiz',
    'information' => [
        '1' => 'Umumiy ma’lumotlar',
        '2' => 'Ro’yxatdan o’tish',
        '3' => 'Raqamni olish',
        '4' => 'Musobaqa kunida',
        '5' => 'Viza va mehmonxonalar',
    ],
    'passed' => [
        '1' => 'Ro‘yxatdan o‘tish <br> muvaffaqiyatli yakunlandi!',
        '2' => 'Sizning shaxsiy kabinetingizda siz qatnashadigan poyga haqida to‘liq ma’lumot doim mavjud bo‘ladi. Shuningdek, u yerda shaxsiy ma’lumotlaringizni o‘zgartirishingiz ham mumkin.',
        '3' => 'SHAXSIY KABINET',
    ],
    'release' => 'Matbuot relizlari',
    'timer' => ['days' => 'KUN','hours' => 'Soat','minute' => 'Daqiqa','second' => 'Soniya'],
];
